package 
{
	import bridges.BlueBridge;
	import bridges.IBridge;
	import bridges.RedBridge;
	import flash.display.Sprite;
	
	public class Main extends Sprite 
	{		
		public function Main():void 
		{
			var bridge:IBridge;
			
			bridge = new BlueBridge();
			bridge.engage();
			
			// We switch bridge
			
			bridge = new RedBridge();
			bridge.engage();			
		}
	}
}