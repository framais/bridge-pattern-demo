package bridges 
{
	public interface IBridge 
	{		
		function engage():void;		
	}
}